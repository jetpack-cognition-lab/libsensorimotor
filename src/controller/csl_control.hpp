#ifndef SUPREME_CSL_CONTROL_HPP
#define SUPREME_CSL_CONTROL_HPP

#include "../common/modules.h"
#include "../common/log_messages.h"

namespace supreme {

inline float pos(float value) { return std::max(.0f, value); }
inline float neg(float value) { return std::min(.0f, value); }
inline float posneg(float value, float p, float n) { return p*pos(value) + n*neg(value); }

namespace csl_defaults {
    const float Umax = 0.5f; /* 50% */
    const float dt100Hz = .01f; /*=100Hz*/
    const float feedback_gain = 1.01f;
    const float stall_detect_vel_max = 0.010f;
}

class csl_control
{

public:
    const uint8_t id;  // motor id, just for reference
    const float dt;    // delta_t, e.g. 100Hz = 0.01
    float Umax;        // limit of output voltage
    float noise_level;

    float target_mode = 0;
    float target_feedback = csl_defaults::feedback_gain;

    /* max. position soft limits with inner(0) and outer(1) value:
       CSL will not push further than these angle limits.*/
    struct Position_Limits_t {
        float lo_0 = -0.5f,
              lo_1 = -1.0f,
              hi_0 = +0.5f,
              hi_1 = +1.0f;

        void set(float l0, float l1, float h0, float h1) { lo_0 = l0; lo_1 = l1; hi_0 = h0; hi_1 = h1; }

    } p_lim;

    float gi_pos =  2.5f;  // input gain for release, hold + contraction mode
    float gi_neg = 10.0f;  // input gain for support mode


    csl_control( uint8_t id = 0
               , float dt = csl_defaults::dt100Hz
               , float Umax = csl_defaults::Umax
               , float noise_level = .0f
               )
    : id(id)
    , dt(dt)
    , Umax(Umax)
    , noise_level(noise_level)
    , u_lim_hi(+Umax) // adaptive voltage limits for stall detection
    , u_lim_lo(-Umax)
    , tau(csl_defaults::stall_detect_vel_max/2)
    , eta(csl_defaults::stall_detect_vel_max)
    {
    }

    float step(float /*current position=*/p, float /*velocity input*/ in = .0f)
    {
        /* check position limits */
        float dec = .0f;
        if (p > p_lim.hi_0 and z > .0f) dec = (p - p_lim.hi_0) / (p_lim.hi_1 - p_lim.hi_0);
        if (p < p_lim.lo_0 and z < .0f) dec = (p - p_lim.lo_0) / (p_lim.lo_1 - p_lim.lo_0);

        z = (1.f - tau * clip(dec, .0f, 1.f)) * z; // decay z, if limit was exceed


        float u = clip(-gi * p + z + gi*in*dt, u_lim_lo, u_lim_hi);

        if (u + eps >= u_lim_hi) u_lim_hi += (- eps - u_lim_hi) * eta;
        else                     u_lim_hi += ( Umax - u_lim_hi) * eta * 2.f;

        if (u - eps <= u_lim_lo) u_lim_lo += (  eps - u_lim_lo) * eta;
        else                     u_lim_lo += (-Umax - u_lim_lo) * eta * 2.f;

        update_mode(); /* if necessary, change parameters here for z to have correct value */

        z = gi * p + gf * u;

        if (noise_level != 0.f) z += random_value(-noise_level,+noise_level);
        return u;
    }

    void reset(float p) {
        update_mode();
        z = gi * p; /* set initial conditions */
    }

    float get_mode(void) const { return mode; }

    void set_trigger_velocity_angle_limits(float t) { tau = clip(t, 0.f, csl_defaults::stall_detect_vel_max); }
    void set_trigger_velocity_stall_detect(float e) { eta = clip(e, 0.f, csl_defaults::stall_detect_vel_max); }

private:

    void update_mode()
    {
        if (target_mode != mode) {
            mode = clip(target_mode);
            gi = posneg(mode, gi_pos, gi_neg);
            gf = target_feedback * pos(mode);
        }
    }

    float z = .0f;    // CSL internal state
    float mode = .0f; // mode: -1 support, 0: release 1: contraction

    float gi = .0f;   // input gain
    float gf = .0f;   // feedback gain

    float u_lim_hi;   // adaptive voltage limits...
    float u_lim_lo;   // ...for stall detection

    /* adaptation rates */
    const float eps = 0.010f; // limit margin to safely detect if Umax is reached

    float tau; // decay rate for angle limit
    float eta; // limit adaption rate for stall detection

};

} /* namespace supreme */

#endif /* SUPREME_CSL_CONTROL_HPP */
